<html>
  <head>
    <title>Resume</title>
    <link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>

  <body>
    <div id="section-left">
      <div class="section intro">
        <div class="logo">
            <img src="ProPic.jpg" style = "width: 100px; height: 100px; position: center; top: 20px; left: 20px;">
        </div>
       
        <h1>Kenneth Allyson Dela Cruz</h1>
        <div class="content">
            <span class="dob">28 Jan 19XX</span> <br>
            <span class="intro">Developer</span> <br>
            <span class="title">GoMommy</span>
          </div>
          <div id="contact">
  
            <span class="num">09431234561</span>
            
            <div class="email">
                <a href="mailto:kadc@gmial.com">kadc@gmail.com</a>
              </div>
              <div class="web">
                <i class="fa fa-link" aria-hidden="true"></i>
                <a href="https://websiteexample.com">www.websiteexample.com</a>
              </div>
           </div>
        </div>
      </div>
  
      <div id="section-right">
        <div class="wrapper">
          <div class="section">
            <div class="title">
                Profile
              </div>
              <p>I am a beginner developer.</p>
        </div>

        <div class="section">
          <div class="title">
            Experience
          </div>
          <div class="content">
            <h2>Web Developer</h2>
            <h3>Google | AUG 2018 &raquo; Current</h3>
            <ul>
              <li>Develop feature for the platform</li>
              <li>Develop design for the platform</li>
            </ul>
          </div>
          <div class="content">
            <h2>Multimedia Editor</h2>
            <h3>Fox Entertainment | JUL 2018 &raquo; Current</h3>
            <ul>
                <li>Multimedia Editor feature for the platform</li>
                <li>Graphic Designer for the platform</li>
              </ul>
            </div>
        </div>

        <div class="section">
          <div class="title">
            Education
          </div>
          <div class="content">
            <li>Primary</li> <h3> Leon A Garcia Elementary School</h3>
            <li>Secondary</li> <h3>Daniel R. Aguinaldo National High School</h3>
            <li>Tertiary</li> <h3>University of Southeastern Philippines</h3>
          </div>

        <div class="section">
            <div class="title">
            Skills
          </div>
          <ul>
            <li>Programming.</li>
            <li>Photo Editing.</li>
            <li>Project Management.</li>
        </ul>
      </div>
      <div class="section">
        <div class="title">
            Interest
        </div>
        <div class="content">
            <li>E-Sports</li> 
            <li>Travel </li>
            <li>Food </li>
            <li>Books </li>
            
          </div>
        </div>
      </div>
    </div>
</div>
</body>
</html>